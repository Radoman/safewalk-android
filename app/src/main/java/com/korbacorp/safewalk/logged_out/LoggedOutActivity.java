package com.korbacorp.safewalk.logged_out;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.korbacorp.safewalk.R;
import com.korbacorp.safewalk.logged_out.view.login.LoginFragment;
import com.korbacorp.safewalk.logged_out.view.register.RegisterFragment;

public class LoggedOutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_out);

        init();
    }



    public void init() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_logged_out_container, new LoginFragment());
        fragmentTransaction.commit();

    }

}
