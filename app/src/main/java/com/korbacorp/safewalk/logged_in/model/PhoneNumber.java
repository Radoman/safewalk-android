package com.korbacorp.safewalk.logged_in.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhoneNumber {

    @SerializedName("phonenu")
    @Expose
    private String phonenu;

    public String getPhonenu() {
        return phonenu;
    }

    public void setPhonenu(String phonenu) {
        this.phonenu = phonenu;
    }

}
