package com.korbacorp.safewalk.logged_in.view.settings;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.korbacorp.safewalk.R;
import com.korbacorp.safewalk.logged_in.model.PhoneNumber;
import com.korbacorp.safewalk.logged_out.model.User;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SettingsRecyclerAdapter extends RecyclerView.Adapter<SettingsRecyclerAdapter.ViewHolder> {

    private List<PhoneNumber> phonesList;
    private Activity act;
    private User user;

    SettingsRecyclerAdapter(Activity act, List<PhoneNumber> phonesList, User user){
        this.phonesList = phonesList;
        this.act = act;
        this.user = user;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_settings, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        final String phone = phonesList.get(i).getPhonenu();
        viewHolder.textPhone.setText(phone);

        viewHolder.buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteNumber(phone);
            }
        });

    }

    @Override
    public int getItemCount() {
        return phonesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textPhone;
        ImageView buttonDelete;

        ViewHolder(View itemView) {
            super(itemView);
            textPhone = itemView.findViewById(R.id.textSettingsPhone);
            buttonDelete = itemView.findViewById(R.id.imageDeletePhone);
        }
    }

    public void deleteNumber(final String number) {
        RequestQueue queue = Volley.newRequestQueue(act);
        final Gson gson = new Gson();

        final String url = "http://172.16.27.31:3000/removeNumber.api";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response: ", response);
                        Gson gson = new Gson();
                        Type listType = new TypeToken<ArrayList<PhoneNumber>>(){}.getType();
                        phonesList = gson.fromJson(response.toString(), listType);
                        notifyDataSetChanged();

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("phoneNumber", number);
                params.put("userID", user.getId().toString());

                return params;
            }
        };
        queue.add(postRequest);

    }
}
