package com.korbacorp.safewalk.logged_in.view.tracker;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.korbacorp.safewalk.PermissionUtils;
import com.korbacorp.safewalk.R;
import com.korbacorp.safewalk.logged_in.LoggedInActivity;
import com.korbacorp.safewalk.logged_in.service.LocationService;
import com.korbacorp.safewalk.logged_in.util.SharedPref;
import com.korbacorp.safewalk.logged_in.view.settings.SettingsFragment;
import com.korbacorp.safewalk.logged_out.model.User;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrackerFragment extends Fragment implements LocationListener {

    Button buttonTrack, buttonOk, buttonStop;
    LinearLayout linearPingContainer;
    double lat, lng;
    LocationManager locationManager;
    String mprovider;
    User user;



    public TrackerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_tracker, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ImageView imageSettings = view.findViewById(R.id.imageSettings);
        imageSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout_logged_in_container, new SettingsFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        buttonOk = view.findViewById(R.id.buttonOk);
        buttonTrack = view.findViewById(R.id.buttonTrack);
        buttonStop = view.findViewById(R.id.buttonStop);
        linearPingContainer = view.findViewById(R.id.linearPingContainer);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        mprovider = locationManager.getBestProvider(criteria, false);

        if (mprovider != null && !mprovider.equals("")) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = locationManager.getLastKnownLocation(mprovider);
            locationManager.requestLocationUpdates(mprovider, 15000, 1, this);

            if (location != null)
                onLocationChanged(location);
            else
                Toast.makeText(getActivity(), "No Location Provider Found Check Your Code", Toast.LENGTH_SHORT).show();
        }


        Gson gson = new Gson();
        user = gson.fromJson(SharedPref.getStringPreference(getActivity(),"USER"), User.class);
        Log.d("USER111", user.getId().toString());

        buttonTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest("start");
                buttonTrack.setVisibility(View.INVISIBLE);
                linearPingContainer.setVisibility(View.VISIBLE);
            }
        });

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest("ping");
            }
        });

        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendRequest("end");
                buttonTrack.setVisibility(View.VISIBLE);
                linearPingContainer.setVisibility(View.INVISIBLE);
            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.logged_in_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if(itemId == R.id.tracker_menu_settings) {

        }

        return super.onOptionsItemSelected(item);


    }

    public void sendRequest(String method) {

        Log.d("GEO: ", ""+ lat);


        RequestQueue queue = Volley.newRequestQueue(getActivity());

        final String url = "http://172.16.27.31:3000/"+method+".api/"+user.getId()+"/"+lat+"/"+lng;

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        try{

                            Log.d("Error.Response", error.getMessage());
                        }catch(Exception e) {
                            Log.d("Error.Response", e.getMessage());

                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("userID", user.getId().toString());
                params.put("longitude", lng+"");
                params.put("latitude", lat+"");

                return params;
            }
        };
        queue.add(postRequest);

    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();

        Log.d("GEO: ", "LAT:LNG "+lat+" "+lng);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
